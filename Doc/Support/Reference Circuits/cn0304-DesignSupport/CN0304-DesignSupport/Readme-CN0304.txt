Analog Devices Inc.
Design Support Package
CN0304
04/23/2014
Rev. 0

This file is designed to provide you with a brief overview of what is contained within the circuit note design support package.


**********************
*******Overview*******
**********************


CN0304 Design Support Package contains: 
	
	Link to Circuit Note
	Link to Product Datasheets
	Schematics
	Bill of Materials	
	Gerber Layout Files
	PADS Layout Files
	Assembly Drawings
	Link to LabVIEW Software for Circuit Note	
	Link to Symbols and Footprints
	Link to Technical Support
	Link to Other Resources



**********************
***File Explanation**
**********************


Circuit Note - Copy of the circuit note this design support package was made for.  This file is provided as a PDF:
	CN0304: http://www.analog.com/CN0304


Product Datasheets - Copy of all ADI component datasheets used in the circuit.  These files are provided as a PDF:
	AD9834:   	http://www.analog.com/AD9834
	AD8014:		http://www.analog.com/AD8014
	


Circuit Note Schematic: Provided as a .pdf file and an PADS .sch file:
	EVAL-CN0304-SDZ-PADSSchematic-RevA.pdf
	EVAL-CN0304-SDZ-PADSSchematic-RevA.sch


Bill of Material -  A complete list of components used within the circuit.  Details the quantity, value, and manufacturer information.  This file is provided in an Excel format:
	EVAL-CN0304-SDZ-BOM-RevA.xlsx

Evaluation Software for AD9834 -- Includes LabVIEW .exe file, DLL, and instructions for running the software in the LabVIEW environment: 
	ftp://ftp.analog.com/pub/evalcd/AD9834

AD9834 Evaluation Board User Guide, UG-266:
	http://www.analog.com/UG-266

SDP-B Evaluation Board (EVAL-SDP-CB1Z):
	http://www.analog.com/EVAL-SDP-CB1Z


Gerber layout files - Layout files provided in Gerber and .pdf format within the .zip file:
	EVAL-CN0304-SPZ-GBR-RevA.zip

PADs layout files:
	EVAL-CN0304-SDZ-PADSLayout-RevA.pcb
	

Assembly Drawings - Provided as a .pdf file:
	EVAL-CN0304-SDZ-Assembly-RevA.pdf

XY Placement:

	EVAL-CN0304-XYPlacement-RevA.xlsx


Symbols and Footprints for ADI parts:

AD9834:
http://www.analog.com/en/digital-to-analog-converters/direct-digital-synthesis-dds/ad9834/products/symbols-footprints.html

AD8014
http://www.analog.com/en/all-operational-amplifiers-op-amps/operational-amplifiers-op-amps/ad8014/products/symbols-footprints.html?location=Symbols



Technical Support -  If you require further technical assistance please contact us by phone, email, or our EngineerZone community.  http://www.analog.com/en/content/technical_support_page/fca.html



**********************
***Other Resources****
**********************


Resources that are not provided by Analog Devices, but could be helpful.


Gerber Viewer:  http://www.graphicode.com


PADs Viewer: http://www.mentor.com/products/pcb-system-design/design-flows/pads/pads-pcb-viewer


Allegro Physical Viewer: http://www.cadence.com/products/pcb/Pages/downloads.aspx



